import { Button } from "@/components/ui/button";

export default function Hero() {
  return (
    <div className="relative container h-screen flex justify-center items-center overflow-hidden">
      {/* ------------------------------------------ */}
      <div
        className="absolute inset-x-0 -top-40 -z-10 transform-gpu overflow-hidden blur-3xl sm:-top-80"
        aria-hidden="true"
      >
        <div
          className="relative left-[calc(50%-11rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 rotate-[30deg] bg-gradient-to-tr from-[#ff80b5] to-[#9089fc] opacity-30 sm:left-[calc(50%-30rem)] sm:w-[72.1875rem]"
          style={{
            clipPath:
              "polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)",
          }}
        />
      </div>
      {/* <div
        className="absolute inset-x-0 sm:right-2 -top-40 -translate-x-[80%] -z-10 transform-gpu overflow-hidden blur-3xl"
        aria-hidden="true"
      >
        <div
          className="relative left-[calc(50%-11rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 rotate-[30deg] bg-gradient-to-tr from-[#ff80b5] to-[#9089fc] opacity-30 sm:left-[calc(50%-30rem)] sm:w-[72.1875rem]"
          style={{
            clipPath:
              "polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)",
          }}
        />
      </div> */}
      {/* ------------------------------------------ */}
      <div className=" text-center gap-3 flex justify-center flex-col items-center z-20">
        <h1 className="text-4xl font-bold tracking-tight sm:text-6xl">
          Infinite Project Manager
        </h1>
        <p className=" text-base sm:text-xl w-full sm:w-3/4">
          Break down your projects into manageable tasks. Prioritize, assign,
          and track progress effortlessly, making sure nothing falls through the
          cracks.
        </p>
        <div className="flex justify-center w-full gap-3 sm:gap-8">
          <Button>Get Started</Button>
          <Button
            variant="outline"
            className="dark:bg-slate-800  border border-slate-900"
          >
            Learn More
          </Button>
        </div>
      </div>
    </div>
  );
}
