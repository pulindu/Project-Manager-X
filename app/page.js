import Hero from "@/components/Hero";
import Navigation from "@/components/Navigation";

export default function Home() {
  return (
    <main className="">
      {/* <Navbar /> */}
      <Navigation />
      <Hero />
    </main>
  );
}
